## importamos las librerias que vamos a utilizar.
import numpy as np
import matplotlib.pyplot as plt
from datetime import date

def grafic_create(data, fileName, param):
    # Definimos Parametros para nombrar imagen.
    today = date.today()
    ext = ".png"
    dir = "src/images/" + fileName + "/"
    
    nameFile = dir  + param

    # Nombramos la imagen. 
    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 13
    fig_size[1] = 8
    plt.rcParams["figure.figsize"] = fig_size

    if param == "Temperature":
        # Creamos figuras que contenga un solo eje.
        fig, ax = plt.subplots()

        # Dibujamos la linea con los parametros anteriores @params1 y @params2.
        plt.title(param)
        ax.plot(data[0][1], data[0][0], label="Point3", color="#1569ad", linewidth=1.5)
        ax.plot(data[1][1], data[1][0], label="Point4", color="#e38120", linewidth=1.5)
        
        # Linea de limites
        ax.axhline(y=80, color="red", linestyle="--")

        # Leyendas en base al eje X
        ax.set_xlabel('time - 10 days')

        # Leyendas en base al eje Y
        ax.set_ylabel(param + '- C°')
        plt.legend()
        plt.savefig(nameFile + ext)

    else:
        fig, ax = plt.subplots()
        plt.title(param)
        ax.plot(data[0][0][1], data[0][0][0], label="H - Point3", color="#b8bcff", linewidth=1)
        ax.plot(data[1][0][1], data[1][0][0], label="V - Point3", color="#454cbf", linewidth=1, linestyle="--")
        ax.plot(data[2][0][1], data[2][0][0], label="A - Point3", color="#101566", linewidth=1, linestyle=":")
        ax.plot(data[0][1][1], data[0][1][0], label="H - Point4", color="#ffd191", linewidth=1)
        ax.plot(data[1][1][1], data[1][1][0], label="V - Point4", color="#e69b32", linewidth=1, linestyle="--")
        ax.plot(data[2][1][1], data[2][1][0], label="A - Point4", color="#9e6009", linewidth=1, linestyle=":")

        # Leyendas en base al eje X
        ax.set_xlabel('time - 10 days')

        # Leyendas en base al eje Y
        if param == "Velocity":
            ax.set_ylabel(param + " - mm/s") 
            ax.axhline(y=10, color="red", linestyle="--")
        else:
            if param == "Acceleration":
                ax.set_ylabel(param + " - g")
            else: 
                ax.set_ylabel(param + " - gE")
            ax.axhline(y=1.5, color="red", linestyle="--")
        plt.legend()
        fig.savefig(nameFile + ext)

    # Si queremos mostrar el gráfico solo tenemos que descomentar.
    # plt.show();