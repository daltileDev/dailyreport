from docx import Document
from docx.shared import Cm

def create_doc(models):
    params = ["Temperature", "Velocity", "AccelerationEnv", "Acceleration"]
    # Creación del documento
    document = Document()

    # Añadimos un titulo al documento, a nivel 0
    document.add_heading('Reporte Diario', 0)

    # Añadimos un párrafo
    p = document.add_paragraph('A continuación se adjunta los valores obtenidos:')

    # Imágenes
    for m, name in models:
        document.add_heading(name, level=1)
        for param in params:
            document.add_picture("src/images/" + name + "/" + param + ".png", width=Cm(14))

    document.save('dailyReport.docx')