import smtplib, ssl, os
from email.mime import base, multipart, text, image
from email import encoders  
from json_environ import Environ

MIMEBase, MIMEMultipart = base.MIMEBase, multipart.MIMEMultipart
MIMEText, MIMEImage = text.MIMEText, image.MIMEImage

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath("varEnvironment.json")))
env_path = os.path.join(BASE_DIR+"/dailyReport/", 'varEnvironment.json')
env = Environ(path=env_path)

sender = env("SENDER")  
receiver = env("RECEIVER") 
smtp_server = env('SERVERMAIL')  
port = env("PORT")

def send_email():

    message = MIMEMultipart('alternative')  
    message['Subject'] = 'Reporte diario'  
    message['From'] = sender  
    message['To'] = receiver  
    message.attach(MIMEText('A continuación se adjuntan los reportes del día de hoy', 'utf-8'))

    image_file_path = "src/files/dailyReport.docx"
    image_file = open(image_file_path, 'rb')
    part = MIMEBase('application', 'octet-stream')
    part.set_payload(image_file.read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', "attachment; filename=dailyReport.docx")
    message.attach(part)

    with smtplib.SMTP(smtp_server, port) as server:  
        server.sendmail(sender, receiver, message.as_string())
        