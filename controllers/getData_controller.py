import pandas as pd
import numpy as np
from accessConect import connectionDb
from datetime import date
import datetime
from controllers.graficView import grafic_create

g = globals()
today = date.today()
rangeDay = today - datetime.timedelta(days=10)

def var_create(numero, cadena='var', lista_valores=None, inicio=1 ):
    fin = inicio + numero
    for i, j in enumerate(range(inicio, fin)):
        if lista_valores:
            valor = lista_valores[i]
        else:
            valor = None
        g[cadena+str(j)] = valor

def requestDB(model):
    query = "SELECT `PointIndex`, `Units`, `Axis`, `RealValue`, `Date_Time` FROM "+ model +" ORDER BY `Date_Time` DESC"
    return query

def machines(model, fileName): 
    query = requestDB(model)
    data = connectionDb(query)
    a = np.array(data)
    pointIndex, units, axis, val, time  = np.hsplit(a, 5)
    df = pd.DataFrame({
        'pointIndex':pointIndex[:,0],
        'units': units[:,0],
        'axis': axis[:,0],
        'val': val[:,0],
        'date': pd.to_datetime(time[:,0])
        })
    filtered_df =df.loc[df["date"].between(rangeDay.strftime("%m/%d/%Y"), today.strftime("%m/%d/%Y"))]
    filter_points = [filtered_df.query('pointIndex == 1'), filtered_df.query('pointIndex == 2')]
    filter_poinTemp = [filter_points[0].query('units == 20'), filter_points[1].query('units == 20')]
    filter_axis = [filter_poinTemp[0].query('axis == -1'), filter_poinTemp[1].query('axis == -1')]
    tempData = np.array([[filter_axis[0].val, filter_axis[0].date], 
                [filter_axis[1].val, filter_axis[1].date]])
    grafic_create(tempData, fileName, "Temperature")

    params = [["2", "Velocity"],["6", "AccelerationEnv"],["0", "Acceleration"]]
    for n, param in params:
        filter_units = [filter_points[0].query('units == ' + n), filter_points[1].query('units == ' + n)]
        h_axis = [filter_units[0].query('axis == 1'), filter_units[1].query('axis == 1')]
        v_axis = [filter_units[0].query('axis == 2'), filter_units[1].query('axis == 2')]
        a_axis = [filter_units[0].query('axis == 3'), filter_units[1].query('axis == 3')]
        unitData = [
            [[h_axis[0].val,
              h_axis[0].date],
             [h_axis[1].val,
              h_axis[1].date]],
            [[v_axis[0].val,
              v_axis[0].date],
             [v_axis[1].val, 
              v_axis[1].date]],
            [[a_axis[0].val,
              a_axis[0].date],
             [a_axis[1].val,
              a_axis[1].date]]]
        grafic_create(unitData, fileName, param)