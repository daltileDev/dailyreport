## importamos las librerias que vamos a utilizar.
import numpy as np
import matplotlib.pyplot as plt
from datetime import date
# from controllers.getData_controller import machines
# from controllers.email_controller import send_email
from controllers.test import create_doc

models = [
    ["`m__155107520`", "LWM1-013-I_AirCalCA-H1"],
    ["`m__738019081`", "LWM1-011-I_CombAux-H1"],
    ["`m__493062266`", "LWM1-010-I_CombCA-H1"]
]

# for model, fileName in models:
    # data = machines(model, fileName)

create_doc(models)
# send_email(models)
