import mysql.connector
import os
from json_environ import Environ

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath("varEnvironment.json")))
env_path = os.path.join(BASE_DIR+"/dailyReport/", 'varEnvironment.json')
env = Environ(path=env_path)

db = mysql.connector.connect(
  host = env('HOST'),
  user = env('USER'),
  password = env('PASS'),
  db = env('DB')
)
cursor = db.cursor()

def connectionDb(query):
    ## getting records from the table
    cursor.execute(query)

    ## fetching all records from the 'cursor' object
    records = cursor.fetchall()
    return records
